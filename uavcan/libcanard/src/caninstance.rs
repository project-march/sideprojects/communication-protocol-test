use crate::libcanard::*;

type AllocatorFn = unsafe extern "C" fn(*mut CanardInstance, usize) -> *mut cty::c_void;
type DeallocatorFn = unsafe extern "C" fn(*mut CanardInstance, *mut cty::c_void) -> ();

pub enum MTUOptions {
    CanClassic,
    CanFD,
}

impl MTUOptions {
    fn to_value(&self) -> u8 {
        match self {
            MTUOptions::CanClassic => CANARD_MTU_CAN_CLASSIC as u8,
            MTUOptions::CanFD => CANARD_MTU_CAN_FD as u8,
        }
    }
}

/// Allows for the construction of a CAN instance.
/// # Example
/// ```
/// let builder = CanInstanceBuilder::default()
///     .allocator(allocate_wrapper)
///     .deallocator(deallocate_wrapper)
///     .mtu(MTUOptions::CanClassic)
///     .node_id(1);
/// // Transform the builder into an actual CanInstance.
/// unsafe { builder.create() }
/// ```
pub struct CanInstanceBuilder {
    allocator: Option<AllocatorFn>,
    deallocator: Option<DeallocatorFn>,
    node_id: Option<CanardNodeID>,
    default_outgoing_mtu: MTUOptions,
}

impl Default for CanInstanceBuilder {
    /// Create a default instance of the CanInstanceBuilder.
    fn default() -> Self {
        CanInstanceBuilder {
            allocator: None,
            deallocator: None,
            node_id: None,
            default_outgoing_mtu: MTUOptions::CanClassic,
        }
    }
}

impl CanInstanceBuilder {
    /// Provide a valid allocator function that the CAN instance should use. This should be an
    /// O(1) allocator.
    /// # Example
    /// ```
    /// unsafe extern "C" fn allocate_wrapper(_: *mut CanardInstance, size: usize)
    ///     -> *mut cty::c_void {
    ///         ALLOCATOR.allocate(size) as *mut cty::c_void
    /// }
    ///
    /// let builder = CanInstanceBuilder::default()
    ///                 .allocator(allocate_wrapper);
    /// ```
    pub fn allocator(mut self, allocator: AllocatorFn) -> Self {
        self.allocator = Some(allocator);
        self
    }

    /// Provide a valid deallocator function that the CAN instance should use. This should be an
    /// O(1) deallocator.
    /// # Example
    /// ```
    /// unsafe extern "C" fn deallocate_wrapper(_: *mut CanardInstance, ptr: *mut cty::c_void) {
    ///    ALLOCATOR.deallocate(ptr as *mut u8)
    /// }
    ///
    /// let builder = CanInstanceBuilder::default()
    ///                 .deallocator(deallocate_wrapper);
    /// ```
    pub fn deallocator(mut self, deallocator: DeallocatorFn) -> Self {
        self.deallocator = Some(deallocator);
        self
    }

    /// Set the node ID of this local CAN node. Per the UAVCAN Specification, the node ID should not be assigned more than once.
    /// # Example
    /// ```
    /// let builder = CanInstanceBuilder::default().node_id(5);
    /// ```
    pub fn node_id(mut self, node_id: CanardNodeID) -> Self {
        self.node_id = Some(node_id);
        self
    }

    /// The transport-layer maximum transmission unit (MTU).
    /// This setting defines the default maximum number of bytes per CAN data frame in all outgoing transfers.
    /// Regardless of this setting, CAN frames with any MTU can always be accepted. By default, this
    /// is set to MTUOptions::CanClassic.
    /// # Example
    /// ```
    /// let builder = CanInstanceBuilder::default().default_outgoing_mtu(MTUOptions::CanClassic);
    /// ```
    pub fn default_outgoing_mtu(mut self, mtu: MTUOptions) -> Self {
        self.default_outgoing_mtu = mtu;
        self
    }

    /// Transform the builder into an actual CanInstance. This operation is unsafe, because Rust
    /// cannot guarantee that allocator and deallocator functions are valid and working.
    /// All values should have defined, otherwise, a panic will occur.
    pub unsafe fn create(self) -> CanInstance {
        let allocator = self.allocator.unwrap();
        let deallocator = self.deallocator.unwrap();
        let node_id = self.node_id.unwrap();
        let mtu = self.default_outgoing_mtu;

        // The call to canardInit is the only unsafe operation.
        let mut canard_instance = { canardInit(Some(allocator), Some(deallocator)) };
        canard_instance.node_id = node_id;
        canard_instance.mtu_bytes = mtu.to_value().into();

        CanInstance {
            can_instance: canard_instance,
        }
    }
}

pub struct CanInstance {
    can_instance: CanardInstance,
}
