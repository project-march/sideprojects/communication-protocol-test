use crate::ALLOCATOR;
use libcanard::*;

unsafe extern "C" fn allocate_wrapper(
    _: *mut libcanard::CanardInstance,
    size: usize,
) -> *mut cty::c_void {
    ALLOCATOR.allocate(size) as *mut cty::c_void
}

unsafe extern "C" fn deallocate_wrapper(_: *mut libcanard::CanardInstance, ptr: *mut cty::c_void) {
    ALLOCATOR.deallocate(ptr as *mut u8)
}

pub fn initialize() -> CanInstance {
    let builder = CanInstanceBuilder::default()
        .allocator(allocate_wrapper)
        .deallocator(deallocate_wrapper)
        .default_outgoing_mtu(MTUOptions::CanClassic)
        .node_id(1);
    unsafe { builder.create() }
}
