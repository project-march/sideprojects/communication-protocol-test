#![no_std]
#![no_main]

// pick a panicking behavior
use panic_halt as _; // you can put a breakpoint on `rust_begin_unwind` to catch panics
                     // use panic_abort as _; // requires nightly
                     // use panic_itm as _; // logs messages over ITM; requires ITM support
                     // use panic_semihosting as _; // logs messages to the host stderr; requires a debugger
use cortex_m_rt::{entry, heap_start};
//use cortex_m_semihosting::hprintln;
use stm32f4xx_hal::{delay::Delay, prelude::*, stm32};

use o1heap::CortexMO1Heap;

mod uavcan;

#[global_allocator]
pub(crate) static ALLOCATOR: CortexMO1Heap = CortexMO1Heap::empty();

const HEAP_SIZE: usize = 4_096;
#[entry]
fn main() -> ! {
    // Allocate a total of HEAP_SIZE bytes as the heap. This heap behaves in O(1) time,
    // because that is required by UAVCAN.
    unsafe { ALLOCATOR.init(heap_start() as usize, HEAP_SIZE) }

    let can = uavcan::initialize();

    // Claim peripherals
    let peripherals = stm32::Peripherals::take().unwrap();
    let core_peripherals = stm32::CorePeripherals::take().unwrap();

    // Get the GPIOC from the peripherals.
    let gpio = peripherals.GPIOC.split();

    // Set the PC12 pin into a PUSH/PULL output pin.
    let mut led = gpio.pc12.into_push_pull_output();

    // Get the RCC and set the systemclock to 40 MHz.
    let rcc = peripherals.RCC.constrain();
    let clock = rcc.cfgr.sysclk(40.mhz()).freeze();

    let mut delay = Delay::new(core_peripherals.SYST, clock);

    loop {
        led.set_low().unwrap();
        delay.delay_ms(500_u16);
        led.set_high().unwrap();
        delay.delay_ms(500_u16);
    }
}
