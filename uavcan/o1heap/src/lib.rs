//! A O(1) time heap allocator for Cortex-M processors. This is built by linking to the
//! C library "o1heap" from https://github.com/pavel-kirienko/o1heap
//!
//! Note that using this as your global allocator requires nightly Rust.
//!
//! # Example
//!
//! For a usage example, see `examples/global_alloc.rs`.

#![no_std]

use core::alloc::{GlobalAlloc, Layout};
use core::cell::Cell;
use cortex_m::interrupt::Mutex;

mod o1heap;

pub struct CortexMO1Heap {
    heap_mutex: Mutex<Cell<Option<o1heap::O1HeapInstance>>>,
}

impl CortexMO1Heap {
    /// Crate a new UNINITIALIZED heap allocator
    ///
    /// You must initialize this heap using the
    /// [`init`](struct.CortexMO1Heap.html#method.init) method before using the allocator.
    pub const fn empty() -> CortexMO1Heap {
        CortexMO1Heap {
            heap_mutex: Mutex::new(Cell::new(None)),
        }
    }
    /// Initializes the heap.
    ///
    /// This function must be called BEFORE you run any code that makes use of the
    /// allocator.
    ///
    /// `start_addr` is the address where the heap will be located.
    ///
    /// `size` is the size of the heap in bytes.
    ///
    /// Note that:
    ///
    /// - The heap grows "upwards", towards larger addresses. Thus `end_addr` must
    ///   be larger than `start_addr`
    ///
    /// - The size of the heap is `(end_addr as usize) - (start_addr as usize)`. The
    ///   allocator won't use the byte at `end_addr`.
    ///
    /// This function will panic if the provided size is insufficient for the allocator
    /// to function.
    ///
    /// # Unsafety
    ///
    /// Obey these or Bad Stuff will happen.
    ///
    /// - This function must be called exactly ONCE, before anything that might use the allocator.
    /// - `size > 0`
    pub unsafe fn init(&self, start_addr: usize, size: usize) {
        cortex_m::interrupt::free(|cs| {
            // Claim the mutex for this CriticalSection
            let mutex_value = self.heap_mutex.borrow(cs);

            let heap = o1heap::o1heapInit(start_addr as *mut cty::c_void, size, None, None);

            // Specification states that o1heapInit() will return a null pointer if the
            // provided space is insuffient. In such a case, just panic.
            if heap.is_null() {
                panic!("The provided space in insufficient")
            }

            // Move the created heap arena into the Cell that the Mutex protects.
            mutex_value.set(Some(*heap));
        });
    }

    pub unsafe fn allocate(&self, size: usize) -> *mut u8 {
        cortex_m::interrupt::free(|cs| {
            // Claim the mutex for this CriticalSection and take the heap out of it.
            // If the heap is not initialized with init(), this will panic because
            // the Mutex would contain a None value, which cannot be
            // unwrapped.
            let mutex_value = self.heap_mutex.borrow(cs);
            let mut heap = mutex_value.take().unwrap();

            // This function returns a null pointer if something fails during allocation
            // and the documentation of GlobalAlloc specifies that alloc() should
            // also return a null pointer if something fails during allocation.
            // Therefore, the pointer can just be returned as-is.
            let pointer = o1heap::o1heapAllocate(&mut heap, size);

            // Place the taken heap back into the Cell into the Mutex.
            mutex_value.set(Some(heap));
            pointer
        }) as *mut u8
    }

    pub unsafe fn deallocate(&self, ptr: *mut u8) {
        cortex_m::interrupt::free(|cs| {
            // Claim the mutex for this CriticalSection and take the heap out of it.
            // If the heap is not initialized with init(), this will panic because
            // the Mutex would contain a None value, which cannot be
            // unwrapped.
            let mutex_value = self.heap_mutex.borrow(cs);
            let mut heap = mutex_value.take().unwrap();

            o1heap::o1heapFree(&mut heap, ptr as *mut cty::c_void);

            // Place the taken heap back into the Mutex.
            mutex_value.set(Some(heap));
        });
    }
}

unsafe impl GlobalAlloc for CortexMO1Heap {
    unsafe fn alloc(&self, layout: Layout) -> *mut u8 {
        self.allocate(layout.size())
    }

    unsafe fn dealloc(&self, ptr: *mut u8, _layout: Layout) {
        self.deallocate(ptr)
    }
}
